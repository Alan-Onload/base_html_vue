import navegador from '/src/components/navegador.js'

let root = new Vue({
    el: "#root",
    data: {
        email:null,
        password:null

    },
    watch: {


    },
    mounted() {
        M.AutoInit()

    },
    methods: {
        logar() {

            fetch("/login",{
                method:"POST",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({email:this.email,password:this.password})
            })
            .then(res=>res.json())
            .then(data=>{
                if (data) {
                    
                    Swal.fire({
                        icon: 'success',
                        title: "Sucesso",
                        position:"center",
                        text: `Bem-vindo ${data.email}`
        
                    })
                    
                }
            })


        }

    },
    components: {
        navegador

    }
})
