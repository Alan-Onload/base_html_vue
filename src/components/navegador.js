const navegador = {
    props:['icon']
    ,
    data(){
        return{
            style:{
                "width":"100%",
                "height":"20vh"
       
            }

        }

    },
    template:`
    <header :style="style">
        <nav class="nav-wrapper">
            <a href="#" class="brand-logo center">
            <i class="material-icons">{{icon}}</i>
            login
            </a>
        </nav>
    </header>
    
    
    `
}

export default navegador;