<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\PhpRenderer;

class Controller{


    public function view(Request $request,Response $response, $args) {
        $renderer = new PhpRenderer('templates');
        return $renderer->render($response, "index.html", $args);
    }

    public function login(Request $request,Response $response, $args) {
        $body = $request->getParsedBody();
        $response->getBody()->write(json_encode($body,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        // 
        return $response->withHeader("Content-Type","application/json")->withStatus(200);
        
        
    }

}