<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\PhpRenderer;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/public/controller/controller.php';



// Create App
$app = AppFactory::create();

$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();

$app->get('/',Controller::class . ":view")->setName('view');

$app->post('/login',Controller::class . ":login")->setName('login');

$app->run();
